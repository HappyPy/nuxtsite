import { Block, Chunk } from '~/three/helpers/worldGen/generateWorld'
import { assoc, flatten } from 'ramda'
// @ts-ignore
import { notEmpty } from 'ramda-extension'
import { BLOC_UV, BLOCK_TEXTURES, BLOCK_TYPES } from '~/constants/three/blockTypes'
import { Box3, FrontSide, Matrix4, Mesh, MeshLambertMaterial, PlaneBufferGeometry, TextureLoader, Vector3 } from 'three'
import { BufferGeometry } from 'three/src/core/BufferGeometry'
import { BufferGeometryUtils } from 'three/examples/jsm/utils/BufferGeometryUtils.js'

export type BlockGeometry = {
  type: BLOCK_TYPES,
  geometry: BufferGeometry,
  colliders: Box3[]
}

export const generateBufferGeometry = (chunks: Chunk[][], positionsX: number[], positionsZ: number[]): Map<string, BlockGeometry>[][] => {
  const visibleChunks = markVisible(chunks, positionsX, positionsZ)
  const chunkGeometries: Map<string, BlockGeometry>[][] = []
  for (let x = 0; x < visibleChunks.length; x++) {
    if(positionsX.includes(x)){
      chunkGeometries[x] = []
      for (let z = 0; z < visibleChunks[x].length; z++) {
        if(positionsZ.includes(z)){
          const types: Map<string, BlockGeometry> = new Map()
          for (const type in BLOCK_TYPES) {
            const faces = makeFace(flatten(chunks[x][z].blocks), type as any as BLOCK_TYPES)
            if (faces) {
              types.set(type, faces)
            }
          }
          chunkGeometries[x][z] = types
        }
      }
    }
  }
  return chunkGeometries
}

export const mapTextures = (geometries: Map<string, BlockGeometry>[][]): Mesh[][][] => {
  const meshes: Mesh[][][] = []
  for (let x = 0; x < geometries.length; x++) {
    meshes[x] = []
    for (let z = 0; z < geometries[x].length; z++) {
      meshes[x][z] = []
      for (const type in BLOCK_TYPES) {
        const geometry = geometries[x][z].get(type)
        if (geometry) {
          // @ts-ignore
          const texture = new TextureLoader().load(BLOCK_TEXTURES[geometry.type])
          const mesh = new Mesh(geometry.geometry, new MeshLambertMaterial({
            map: texture,
            side: FrontSide
          }))
          mesh.geometry.computeBoundingBox()
          meshes[x][z].push(mesh)
        }
      }
    }
  }
  return meshes
}

export const mapTexturesSingleChunk = (geometries: Map<string, BlockGeometry>): Mesh[] => {
  const meshes: Mesh[] = []
      for (const type in BLOCK_TYPES) {
        const geometry = geometries.get(type)
        if (geometry) {
          // @ts-ignore
          const texture = new TextureLoader().load(BLOCK_TEXTURES[geometry.type])
          const mesh = new Mesh(geometry.geometry, new MeshLambertMaterial({
            map: texture,
            side: FrontSide
          }))
          mesh.geometry.computeBoundingBox()
          meshes.push(mesh)
        }
  }
  return meshes
}

const makeFace = (blocks: Block[], type: BLOCK_TYPES): BlockGeometry | null => {
  const pxGeometry = new PlaneBufferGeometry(100, 100)
  // @ts-ignore
  _mapUv(pxGeometry, BLOC_UV[type].px)
  pxGeometry.rotateY(Math.PI / 2)
  pxGeometry.translate(50, 0, 0)

  const nxGeometry = new PlaneBufferGeometry(100, 100)
  // @ts-ignore
  _mapUv(nxGeometry, BLOC_UV[type].nx)
  nxGeometry.rotateY(-Math.PI / 2)
  nxGeometry.translate(-50, 0, 0)

  const pyGeometry = new PlaneBufferGeometry(100, 100)
  // @ts-ignore
  _mapUv(pyGeometry, BLOC_UV[type].py)
  pyGeometry.rotateX(-Math.PI / 2)
  pyGeometry.translate(0, 50, 0)

  const pzGeometry = new PlaneBufferGeometry(100, 100)
  // @ts-ignore
  _mapUv(pzGeometry, BLOC_UV[type].pz)
  pzGeometry.translate(0, 0, 50)

  const nzGeometry = new PlaneBufferGeometry(100, 100)
  // @ts-ignore
  _mapUv(nzGeometry, BLOC_UV[type].nz)
  nzGeometry.rotateY(Math.PI)
  nzGeometry.translate(0, 0, -50)

  const nyGeometry = new PlaneBufferGeometry(100, 100)
  // @ts-ignore
  _mapUv(nyGeometry, BLOC_UV[type].ny)
  nyGeometry.rotateX(Math.PI / 2)
  nyGeometry.translate(0, -50, 0)

  const matrix = new Matrix4()
  const boxMatrix = new Matrix4()
  const geometries: BufferGeometry[] = []
  const colliders: any[] = []
  for (let i = 0; i < blocks.length; i++) {
    const block = blocks[i]
    // @ts-ignore
    if (block.type === BLOCK_TYPES[type]) {
      boxMatrix.makeTranslation(
        block.cords.x * 100 - 50,
        block.cords.y * 100 - 50,
        block.cords.z * 100 - 50
      )
      const box = new Box3(new Vector3(0, 0, 0), new Vector3(100, 100, 100))
      box.applyMatrix4(boxMatrix)
      colliders.push(box)

      matrix.makeTranslation(
        block.cords.x * 100,
        block.cords.y * 100,
        block.cords.z * 100
      )
      if (block.visibleFaces.px) {
        geometries.push(pxGeometry.clone().applyMatrix4(matrix))
      }
      if (block.visibleFaces.nx) {
        geometries.push(nxGeometry.clone().applyMatrix4(matrix))
      }
      if (block.visibleFaces.pz) {
        geometries.push(pzGeometry.clone().applyMatrix4(matrix))
      }
      if (block.visibleFaces.nz) {
        geometries.push(nzGeometry.clone().applyMatrix4(matrix))
      }
      if (block.visibleFaces.py) {
        geometries.push(pyGeometry.clone().applyMatrix4(matrix))
      }
      if (block.visibleFaces.ny) {
        geometries.push(nyGeometry.clone().applyMatrix4(matrix))
      }
    }
  }

  if (notEmpty(geometries)) {
    return { geometry: BufferGeometryUtils.mergeBufferGeometries(geometries), type, colliders }
  } else {
    return null
  }
}

export const markVisible = (chunks: Chunk[][], positionsX: number[], positionsZ: number[]): Chunk[][] => {
  for (let chunkX = 0; chunkX < chunks.length; chunkX++) {
    if(positionsX.includes(chunkX)){
      for (let chunkZ = 0; chunkZ < chunks[chunkX].length; chunkZ++) {
        if(positionsZ.includes(chunkZ)){
          for (let blockX = 0; blockX < chunks[chunkX][chunkZ].blocks.length; blockX++) {
            for (let blockZ = 0; blockZ < chunks[chunkX][chunkZ].blocks[blockX].length; blockZ++) {
              for (let blockY = 0; blockY < chunks[chunkX][chunkZ].blocks[blockX][blockZ].length; blockY++) {
                const block = chunks[chunkX][chunkZ].blocks[blockX][blockZ][blockY]
                if (block.type === BLOCK_TYPES.AIR) {
                  // --- Y ---
                  if (blockY + 1 <= chunks[chunkX][chunkZ].blocks[blockX][blockZ].length) {
                    const blockPY = chunks[chunkX][chunkZ].blocks[blockX][blockZ][blockY + 1]
                    if (blockPY && blockPY.type !== BLOCK_TYPES.AIR) {
                      blockPY.visibleFaces = assoc('ny', true, blockPY.visibleFaces)
                    }
                  }
                  if (blockY - 1 >= 0) {
                    const blockNY = chunks[chunkX][chunkZ].blocks[blockX][blockZ][blockY - 1]
                    if (blockNY && blockNY.type !== BLOCK_TYPES.AIR) {
                      blockNY.visibleFaces = assoc('py', true, blockNY.visibleFaces)
                    }
                  }

                  // --- NX ---
                  if (blockX != 15) {
                    if (chunks[chunkX][chunkZ].blocks[blockX + 1]) {
                      const blockPX = chunks[chunkX][chunkZ].blocks[blockX + 1][blockZ][blockY]
                      if (blockPX && blockPX.type !== BLOCK_TYPES.AIR) {
                        blockPX.visibleFaces = assoc('nx', true, blockPX.visibleFaces)
                      }
                    }
                  } else {
                    if (chunks[chunkX + 1] && chunks[chunkX + 1][chunkZ].blocks[blockX - 15]) {
                      const blockPX = chunks[chunkX + 1][chunkZ].blocks[blockX - 15][blockZ][blockY]
                      if (blockPX && blockPX.type !== BLOCK_TYPES.AIR) {
                        blockPX.visibleFaces = assoc('nx', true, blockPX.visibleFaces)
                      }
                    }
                  }

                  // --- PX ---
                  if (blockX != 0) {
                    if (chunks[chunkX][chunkZ].blocks[blockX - 1]) {
                      const blockNX = chunks[chunkX][chunkZ].blocks[blockX - 1][blockZ][blockY]
                      if (blockNX && blockNX.type !== BLOCK_TYPES.AIR) {
                        blockNX.visibleFaces = assoc('px', true, blockNX.visibleFaces)
                      }
                    }
                  } else {
                    if (chunks[chunkX - 1]) {
                      const blockNX = chunks[chunkX - 1][chunkZ].blocks[blockX + 15][blockZ][blockY]
                      if (blockNX && blockNX.type !== BLOCK_TYPES.AIR) {
                        blockNX.visibleFaces = assoc('px', true, blockNX.visibleFaces)
                      }
                    }
                  }

                  // --- NZ ---
                  if (blockZ != 15) {
                    if (chunks[chunkX][chunkZ].blocks[blockX][blockZ + 1]) {
                      const blockPZ = chunks[chunkX][chunkZ].blocks[blockX][blockZ + 1][blockY]
                      if (blockPZ && blockPZ.type !== BLOCK_TYPES.AIR) {
                        blockPZ.visibleFaces = assoc('nz', true, blockPZ.visibleFaces)
                      }
                    }
                  } else {
                    if (chunks[chunkX][chunkZ + 1] && chunks[chunkX][chunkZ + 1].blocks[blockX][blockZ - 15]) {
                      const blockPZ = chunks[chunkX][chunkZ + 1].blocks[blockX][blockZ - 15][blockY]
                      if (blockPZ && blockPZ.type !== BLOCK_TYPES.AIR) {
                        blockPZ.visibleFaces = assoc('nz', true, blockPZ.visibleFaces)
                      }
                    }
                  }

                  // --- PZ ---
                  if (blockZ != 0) {
                    if (chunks[chunkX][chunkZ].blocks[blockX][blockZ - 1]) {
                      const blockNZ = chunks[chunkX][chunkZ].blocks[blockX][blockZ - 1][blockY]
                      if (blockNZ && blockNZ.type !== BLOCK_TYPES.AIR) {
                        blockNZ.visibleFaces = assoc('pz', true, blockNZ.visibleFaces)
                      }
                    }
                  } else {
                    if (chunks[chunkX][chunkZ - 1]) {
                      const blockNZ = chunks[chunkX][chunkZ - 1].blocks[blockX][blockZ + 15][blockY]
                      if (blockNZ && blockNZ.type !== BLOCK_TYPES.AIR) {
                        blockNZ.visibleFaces = assoc('pz', true, blockNZ.visibleFaces)
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return chunks
}


const _mapUv = (geometry: BufferGeometry, uv: number[]) => {
  for (let i = 0; i < 8; i++) {
    // @ts-ignore
    geometry.attributes.uv.array[i] = uv[i]
  }
}
