import { flatten, forEach, identity, map, range, times } from 'ramda'
// @ts-ignore
import { notEmpty } from 'ramda-extension'
import { ImprovedNoise } from 'three/examples/jsm/math/ImprovedNoise.js'
import { BLOCK_TYPES } from '~/constants/three/blockTypes'
// @ts-ignore

const ts = times(identity, 255)
const genIndexes = times(identity)
export type Cords3D = {
  x: number,
  y: number,
  z: number
}

export type Cords2D = {
  x: number,
  z: number
}

export type Chunk = {
  blocks: Block[][][]
  cords: Cords2D
}

export type Block = {
  cords: Cords3D
  type: BLOCK_TYPES
  visibleFaces: {
    py: boolean,
    ny: boolean,
    nx: boolean,
    px: boolean,
    pz: boolean,
    nz: boolean,
  }
}

const invisibleFaces = {
  py: false,
  ny: false,
  nx: false,
  px: false,
  pz: false,
  nz: false
}

const generateHeight = (width: number, height: number): number[] => {
  const data: number[] = [0]
  const perlin = new ImprovedNoise()
  const size = width * height
  let quality = 2
  const z = Math.random() * 100

  for (let j = 0; j < 3; j++) {
    if (j === 0) for (let i = 0; i < size; i++) data[i] = 0
    for (let i = 0; i < size; i++) {
      const x = i % width, y = (i / width) | 0
      data[i] += perlin.noise(x / quality, y / quality, z) * quality

    }
    quality *= 4
  }
  return data

}

const generateBlockColumns = (data: number[], worldDepth: number, worldWidth: number): Block[][][] => {
  const getY = (x: number, z: number): number => {
    return ((data[x + z * worldWidth] * 0.2) | 0) + 50
  }

  const maxHeights = flatten(map(
    (x: number) =>
      map((z: number) => {
          const y = getY(x, z)
          return { x: x, y: y, z: z }
        },
        times(identity, worldDepth)), times(identity, worldWidth)))
  const blocks: Block[][][] = []

  forEach((x) => {
    blocks[x] = []
    forEach((z) => {
      blocks[x][z] = []
    }, genIndexes(worldDepth))
  }, genIndexes(worldWidth))

  forEach((cords: Cords3D) => {
    forEach((y: number) => {
      blocks[cords.x][cords.z][y] = {
        cords: { x: cords.x, y: y, z: cords.z },
        type: BLOCK_TYPES.STONE,
        visibleFaces: invisibleFaces
      }
      return { cords: { x: cords.x, y: y, z: cords.z }, type: BLOCK_TYPES.STONE, visibleFaces: invisibleFaces }
    }, times(identity, cords.y))

    forEach((y: number) => {

      blocks[cords.x][cords.z][y + cords.y + 1] = {
        cords: { x: cords.x, y: y + cords.y + 1, z: cords.z },
        type: BLOCK_TYPES.AIR,
        visibleFaces: invisibleFaces
      }
    }, times(identity, 10))

    blocks[cords.x][cords.z][cords.y] = { cords: cords, type: BLOCK_TYPES.GRASS, visibleFaces: invisibleFaces }
    blocks[cords.x][cords.z][0] = {
      cords: { x: cords.x, y: 0, z: cords.z },
      type: BLOCK_TYPES.BEDROCK,
      visibleFaces: invisibleFaces
    }
  }, maxHeights)
  return blocks
}

const generateChunks = (blocks: Block[][][], worldDepth: number, worldWidth: number): Chunk[][] => {
  const chunks: Chunk[][] = []
  forEach((x: number) => {
    chunks[x] = []
  }, times(identity, Math.ceil(worldWidth / 16)))

  forEach((x: number) => {
    forEach((z: number) => {
      const chunkBlocks: Block[][][] = []

      forEach((x) => {
        chunkBlocks[x] = []
        forEach((z) => {
          chunkBlocks[x][z] = []
        }, genIndexes(worldDepth))
      }, genIndexes(worldWidth))

      forEach((zIndex) => {
        forEach((xIndex) => {
          if (zIndex < worldDepth && xIndex < worldWidth) {
            if (notEmpty(blocks[xIndex][zIndex])) {
              chunkBlocks[xIndex % 16][zIndex % 16] = blocks[xIndex][zIndex]
            }
          }
        }, range(x * 16, x * 16 + 16))
      }, range(z * 16, z * 16 + 16))

      chunks[x][z] = { cords: { x: x, z: z }, blocks: chunkBlocks }
    }, times(identity, Math.ceil(worldWidth / 16)))
  }, times(identity, Math.ceil(worldDepth / 16)))
  return chunks
}

export const generateWorld = (worldDepth: number, worldWidth: number): Chunk[][] => {
  const data = generateHeight(worldWidth, worldDepth)
  const blocks = generateBlockColumns(data, worldDepth, worldWidth)
  return generateChunks(blocks, worldDepth, worldWidth)
}
