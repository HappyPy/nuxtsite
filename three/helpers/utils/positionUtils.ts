import { Vector2, Vector3 } from 'three'

export type BlockWorldScenePosition = {
  chunk: Vector2
  block: Vector3
  posInChunk: Vector3
}


export const getBlockChunk = (position: Vector3): BlockWorldScenePosition => {
  const blockPosX = (position.x + 50 - (position.x + 50) % 100) / 100
  const blockPosZ = (position.z + 50 - (position.z + 50) % 100) / 100
  const blockPosY = (position.y + 50 - (position.y + 50) % 100) / 100
  const chunkX = (blockPosX - blockPosX % 16) / 16
  const chunkZ = (blockPosZ - blockPosZ % 16) / 16

  return {
    chunk: new Vector2(chunkX, chunkZ),
    block: new Vector3(blockPosX, blockPosY, blockPosZ),
    posInChunk: new Vector3(blockPosX % 16, blockPosY, blockPosZ  % 16)
  }
}
