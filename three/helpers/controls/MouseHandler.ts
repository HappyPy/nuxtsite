// @ts-ignore
import Scene from '~/components/three/Scene.vue'
import { flatten, isEmpty } from 'ramda'
import { BoxGeometry, Mesh, MeshBasicMaterial, Vector2 } from 'three'
import { Vector3 } from 'three/src/math/Vector3'
import { getBlockChunk } from '~/three/helpers/utils/positionUtils'
import { Block, Chunk } from '~/three/helpers/worldGen/generateWorld'
import { generateBufferGeometry, mapTexturesSingleChunk } from '~/three/helpers/geometry/blockGroupMesher'
import { BLOCK_TYPES } from '~/constants/three/blockTypes'

export interface MouseHandler {
  update: (delta: number) => void
}

export class MouseHandler {
  private readonly scene: Scene
  private readonly box: Mesh
  private blocksToRomoveFromScene: Mesh[] = []
  private timeToRemoveFromScene: number = 1/65;
  constructor(scene: Scene) {
    this.scene = scene

    document.addEventListener('click', this.onClick)

    this.box = new Mesh(
      new BoxGeometry(101, 101, 101),
      new MeshBasicMaterial({
        opacity: 1.0,
        color: 0x000000,
        wireframe: true
      })
    )
  }

  onClick = (e: MouseEvent) => {
    const aimBlockPosition = this.getAimBlockPosition()
    if (aimBlockPosition) {
      let pos = getBlockChunk(aimBlockPosition)
      if (e.button === 0) {
        this.scene.world[pos.chunk.x][pos.chunk.y].blocks[pos.posInChunk.x][pos.posInChunk.z][pos.posInChunk.y] = makeAir(pos.block)
      }
      if (e.button === 2) {
        const aimBlockPlacePosition= this.getAimBlockPlacePosition()
        if(aimBlockPlacePosition){
          pos = getBlockChunk(aimBlockPlacePosition)
          // @ts-ignore
          const matchedId = document.querySelector('.selected').id.match(/inv-(\d)/);
          // @ts-ignore
          this.scene.world[pos.chunk.x][pos.chunk.y].blocks[pos.posInChunk.x][pos.posInChunk.z][pos.posInChunk.y] = makeBlock(pos.block, matchedId[1])
        }
      }
      const posChunkX = [pos.chunk.x]
      const posChunkZ = [pos.chunk.y]
      if (pos.posInChunk.x === 0 && pos.chunk.x > 0) {
        posChunkX.push(pos.chunk.x - 1)
      }
      if (pos.posInChunk.x === 15) {
        posChunkX.push(pos.chunk.x + 1)
      }
      if (pos.posInChunk.z === 0 && pos.chunk.y > 0) {
        posChunkZ.push(pos.chunk.y - 1)
      }
      if (pos.posInChunk.z === 15) {
        posChunkZ.push(pos.chunk.y + 1)
      }
      const newGeometry = generateBufferGeometry(takeChunksAround(this.scene.world, pos.chunk), posChunkX, posChunkZ)
      for (let posChunkXI = 0; posChunkXI < posChunkX.length; posChunkXI++) {
        for (let posChunkZI = 0; posChunkZI < posChunkZ.length; posChunkZI++) {
          const x = posChunkX[posChunkXI]
          const z = posChunkZ[posChunkZI]
          this.scene.blocks[x][z] = newGeometry[x][z]
          const mesh = mapTexturesSingleChunk(newGeometry[x][z])
          this.blocksToRomoveFromScene.push(this.scene.meshes[x][z])
          this.scene.meshes[x][z] = mesh
          this.scene.scene.add(...flatten(mesh))
          this.timeToRemoveFromScene = 1 / 8
        }
      }
    }
  }

  update = (delta: number) => {
    const aimBlockPosition = this.getAimBlockPosition()
    this.timeToRemoveFromScene -= delta;
    if (aimBlockPosition) {
      this.box.position.copy(aimBlockPosition)
    }
    this.scene.scene.add(this.box)
    if(!isEmpty(this.blocksToRomoveFromScene) && this.timeToRemoveFromScene < 0){
      for (let i = 0; i < this.blocksToRomoveFromScene.length; i++) {
        // @ts-ignore
        this.scene.scene.remove(...this.blocksToRomoveFromScene[i]);
      }
      this.blocksToRomoveFromScene = []
    }
  }

  private getAimBlockPosition = (): Vector3 | null => {
    this.scene.raycaster.setFromCamera({ x: 0, y: 0 }, this.scene.camera)
    // @ts-ignore
    var intersections = this.scene.raycaster.intersectObjects(flatten(this.scene.meshes))
    if (intersections.length > 0) {
      const vector = new Vector3(0, 0, 0)
      if (intersections.length > 0) {
        if (intersections[0].face?.normal.x === 1) {
          vector.x = intersections[0].point.x - 50 - (intersections[0].point.x + 50) % 100
        } else {
          vector.x = intersections[0].point.x + 50 - (intersections[0].point.x + 50) % 100
        }
        if (intersections[0].face?.normal.y === 0) {
          vector.y = intersections[0].point.y + 50 - (intersections[0].point.y + 50) % 100
        } else {
          if (intersections[0].face?.normal.y === 1) {
            vector.y = intersections[0].point.y - 50
          }else{
            vector.y = intersections[0].point.y + 50
          }
        }
        if (intersections[0].face?.normal.z === 1) {
          vector.z = intersections[0].point.z - 50 - (intersections[0].point.z + 50) % 100
        } else {
          vector.z = intersections[0].point.z + 50 - (intersections[0].point.z + 50) % 100
        }
      }
      return vector
    } else {
      return null
    }
  }

  private getAimBlockPlacePosition = (): Vector3 | null => {
    this.scene.raycaster.setFromCamera({ x: 0, y: 0 }, this.scene.camera)
    // @ts-ignore
    var intersections = this.scene.raycaster.intersectObjects(flatten(this.scene.meshes))
    if (intersections.length > 0) {
      const vector = new Vector3(0, 0, 0)
      if (intersections[0].face?.normal.x === -1) {
        vector.x = intersections[0].point.x - 50 - (intersections[0].point.x + 50) % 100
      } else if(intersections[0].face?.normal.x === -1) {
        vector.x = intersections[0].point.x + 100 - (intersections[0].point.x + 50) % 100
      }else{
        vector.x = intersections[0].point.x + 50 - (intersections[0].point.x + 50) % 100
      }
      if (intersections[0].face?.normal.y === 0) {
        vector.y = intersections[0].point.y + 50 - (intersections[0].point.y + 50) % 100
      } else {
        if (intersections[0].face?.normal.y === 1) {
          vector.y = intersections[0].point.y
        } else {
          vector.y = intersections[0].point.y - 50
        }
      }
      if (intersections[0].face?.normal.z === -1) {
        vector.z = intersections[0].point.z - 50 - (intersections[0].point.z + 50) % 100
      } else if(intersections[0].face?.normal.z === -1) {
        vector.z = intersections[0].point.z + 100 - (intersections[0].point.z + 50) % 100
      }else{
        vector.z = intersections[0].point.z + 50 - (intersections[0].point.z + 50) % 100
      }
      return vector
    } else {
      return null
    }
  }
}

const takeChunksAround = (chunks: Chunk[][], position: Vector2): Chunk[][] => {
  const newChunks: Chunk[][] = []
  for (let x = position.x - 1; x <= position.x + 1; x++) {
    if (x >= 0 && x < chunks.length) {
      newChunks[x] = []
      for (let y = position.y - 1; y <= position.y + 1; y++) {
        if (y >= 0 && y < chunks[x].length) {
          newChunks[x][y] = chunks[x][y]
        }
      }
    }
  }
  return newChunks
}

const invisibleFaces = {
  py: false,
  ny: false,
  nx: false,
  px: false,
  pz: false,
  nz: false
}
const makeAir = (pos: Vector3): Block => {
  return {
    cords: {
      x: pos.x,
      y: pos.y,
      z: pos.z
    },
    type: BLOCK_TYPES.AIR,
    visibleFaces: invisibleFaces
  }
}
const makeBlock = (pos: Vector3, type: number): Block => {
  return {
    cords: {
      x: pos.x,
      y: pos.y,
      z: pos.z
    },
    // @ts-ignore
    type: BLOCK_TYPES[BLOCKs[type]],
    visibleFaces: invisibleFaces
  }
}

const BLOCKs = [
  'GRASS',
  'STONE',
  'DIORITE',
  'BEDROCK',
  'WOOL_YELLOW',
  'DIAMOND_ORE',
  'GOLD',
  'LAPIS',
  'CLAY',
  'AIR',
]


