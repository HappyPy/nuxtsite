import * as THREE from 'three'
import { BlockGeometry } from '~/three/helpers/geometry/blockGroupMesher'
import { BLOCK_TYPES } from '~/constants/three/blockTypes'

type CollisionVector = {
  px: boolean,
  nx: boolean,
  pz: boolean,
  nz: boolean,
  py: boolean,
  ny: boolean,
}

export type Player = {
  update: (delta: number, blocks: Map<string, BlockGeometry>[][]) => void
}

export const Player = (camera: THREE.Camera, body: THREE.Mesh, boxMesh: THREE.Mesh): Player => {

  let velocityFactor = 500
  const sprintFactor = 700
  const normalFactor = 500
  const gravity = -500
  const jumpVelocity = 300
  const boundingSphere = new THREE.Sphere()
  const boundingBox = new THREE.Box3()
  const inputVelocity = new THREE.Vector3()
  const euler = new THREE.Euler()
  const quat = new THREE.Quaternion()
  const unselected = '#a2a3a8';
  const selected = '#d3d3d5';
  let selectedInv = 0
  let moveForward = false
  let moveBackward = false
  let moveLeft = false
  let moveRight = false

  let canJump = false

  const PI_2 = Math.PI / 2

  // @ts-ignore
  document.querySelector(`#inv-${selectedInv}`).style.backgroundColor = selected
  // @ts-ignore
  document.querySelector(`#inv-${selectedInv}`).classList.add('selected');


  const onMouseMove = function(event: MouseEvent) {

    const movementX = event.movementX || 0
    const movementY = event.movementY || 0

    camera.rotation.x -= movementY * 0.002
    body.rotation.y = body.rotation.y - movementX * 0.01
    camera.rotation.x = (Math.max(-PI_2, Math.min(PI_2, camera.rotation.x)))
  }

  const onKeyDown = function(event: KeyboardEvent) {
    if(event.key === 'Shift'){
      velocityFactor = sprintFactor
    }
    if(event.key === 'f'){
      // @ts-ignore
      document.querySelector('.scene').requestPointerLock()
    }
    if (event.keyCode >= 49 && event.keyCode <= 57) {
      if(selectedInv != event.keyCode - 49){
        // @ts-ignore
        document.querySelector(`#inv-${selectedInv}`).style.backgroundColor = unselected
        // @ts-ignore
        document.querySelector(`#inv-${selectedInv}`).classList.remove('selected');
        // @ts-ignore
        document.querySelector(`#inv-${event.keyCode - 49}`).style.backgroundColor = selected
        // @ts-ignore
        document.querySelector(`#inv-${event.keyCode - 49}`).classList.add('selected');

        selectedInv = event.keyCode - 49

      }
    }
    switch (event.keyCode) {
      case 38: // up
      case 87: // w
        moveForward = true
        break

      case 37: // left
      case 65: // a
        moveLeft = true
        break

      case 40: // down
      case 83: // s
        moveBackward = true
        break

      case 39: // right
      case 68: // d
        moveRight = true
        break

      case 32: // space
        event.preventDefault()
        if (canJump === true) {
          inputVelocity.y = jumpVelocity
        }
        canJump = false
        break
    }

  }

  const onKeyUp = function(event: KeyboardEvent) {
    if(event.key === 'Shift'){
      velocityFactor = normalFactor
    }
    switch (event.keyCode) {

      case 38: // up
      case 87: // w
        moveForward = false
        break

      case 37: // left
      case 65: // a
        moveLeft = false
        break

      case 40: // down
      case 83: // a
        moveBackward = false
        break

      case 39: // right
      case 68: // d
        moveRight = false
        break

    }

  }

  document.addEventListener('mousemove', onMouseMove, false)
  document.addEventListener('keydown', onKeyDown, false)
  document.addEventListener('keyup', onKeyUp, false)


  const update = (delta: number, chunks: Map<string, BlockGeometry>[][]) => {

    inputVelocity.set(0, inputVelocity.y, 0)
    inputVelocity.y += delta * gravity

    if (inputVelocity.y < 0 && inputVelocity.y < gravity) {
      inputVelocity.y = gravity
    }

    if (moveForward) {
      inputVelocity.z = -velocityFactor
    }
    if (moveBackward) {
      inputVelocity.z = velocityFactor
    }

    if (moveLeft) {
      inputVelocity.x = -velocityFactor
    }
    if (moveRight) {
      inputVelocity.x = velocityFactor
    }

    // Convert velocity to world coordinates
    euler.y = body.rotation.y
    euler.x = body.rotation.x
    euler.order = 'XYZ'
    quat.setFromEuler(euler)
    inputVelocity.applyQuaternion(quat)
    //quat.multiplyVector3(inputVelocity);
    const newBody = body.clone()
    const newBoxMesh = boxMesh.clone()
    newBody.position.y -= delta * inputVelocity.y
    newBody.position.x += delta * inputVelocity.x
    newBody.position.z += delta * inputVelocity.z
    newBoxMesh.position.y -= delta * inputVelocity.y
    newBoxMesh.position.x += delta * inputVelocity.x
    newBoxMesh.position.z += delta * inputVelocity.z
    boundingSphere.copy(newBody.geometry.boundingSphere ?? new THREE.Sphere()).applyMatrix4(newBody.matrixWorld)
    boundingBox.copy(newBoxMesh.geometry.boundingBox ?? new THREE.Box3()).applyMatrix4(newBoxMesh.matrixWorld)

    const collisionV: CollisionVector = {
      px: false,
      nx: false,
      pz: false,
      nz: false,
      py: false,
      ny: false
    }
    const playerX = (body.position.x + 50 - (body.position.x + 50) % 100) / 100
    const playerZ = (body.position.z + 50 - (body.position.z + 50) % 100) / 100
    const playerXChunk = (playerX - playerX % 16) / 16
    const playerZChunk = (playerZ - playerZ % 16) / 16
    for (let x = 0; x < chunks.length; x++) {
      if (x === playerXChunk + 1 || x === playerXChunk - 1 || x === playerXChunk) {
        for (let z = 0; z < chunks[x].length; z++) {
          if (z === playerZChunk + 1 || z === playerZChunk - 1 || z === playerZChunk) {
            for (const type in BLOCK_TYPES) {
              const mesh = chunks[x][z].get(type)
              if (mesh && mesh.colliders) {
                for (let i = 0; i < mesh.colliders.length; i++) {
                  const collider = mesh.colliders[i]
                  {
                    const collision = boundingSphere.intersectsBox(collider)
                    const boxCollision = boundingBox.intersectsBox(collider)
                    if (boxCollision) {
                      if (collider.max.y > boundingBox.min.y && collider.max.y < boundingBox.max.y) {
                        collisionV.ny = true
                        if (inputVelocity.y <= 0) {
                          canJump = true
                        }
                      }
                      if (collider.min.y < boundingBox.max.y && collider.min.y > boundingBox.min.y) {
                        collisionV.py = true
                      }
                    }
                    if (collision) {
                      const minY = boundingSphere.center.y - boundingSphere.radius
                      const maxY = boundingSphere.center.y + boundingSphere.radius
                      const collidingWithSide = (collider.min.y > minY || (collider.max.y > minY + 20 && collider.max.y < maxY))
                      if (collider.max.x < boundingSphere.center.x + boundingSphere.radius && collidingWithSide) {
                        collisionV.px = true
                      }
                      if (collider.min.x > boundingSphere.center.x - boundingSphere.radius && collidingWithSide) {
                        collisionV.nx = true
                      }

                      if (collider.max.z < boundingSphere.center.z + boundingSphere.radius && collidingWithSide) {
                        collisionV.pz = true
                      }
                      if (collider.min.z > boundingSphere.center.z - boundingSphere.radius && collidingWithSide) {
                        collisionV.nz = true
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    if (!collisionV.ny && inputVelocity.y < 0) {
      body.position.y += delta * inputVelocity.y
    }
    if (!collisionV.py && inputVelocity.y > 0) {
      body.position.y += delta * inputVelocity.y
    }else if(collisionV.py){
      inputVelocity.y = 0
    }

    if(body.position.x + delta * inputVelocity.x > 0 && body.position.x + delta * inputVelocity.x < 99 * 100) {
      if (!collisionV.px && inputVelocity.x < 0) {
        body.position.x += delta * inputVelocity.x
      }
      if (!collisionV.nx && inputVelocity.x > 0) {
        body.position.x += delta * inputVelocity.x
      }
    }
    if(body.position.z + delta * inputVelocity.z > 0 && body.position.z + delta * inputVelocity.z < 99 * 100){
      if (!collisionV.pz && inputVelocity.z < 0) {
        body.position.z += delta * inputVelocity.z
      }
      if (!collisionV.nz && inputVelocity.z > 0) {
        body.position.z += delta * inputVelocity.z
      }
    }
  }

  return {update}
}
