# happyPyBitbucketPage

> My PGRF2 project

## Required tools

* [Node](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000/threejs
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## ThreeJs part created as university project PGRF2

## Author

* **Vladimír Zahradník** - *Minecraft clone* - [HappyPy](https://bitbucket.org/HappyPy/nuxtsite/src/master/)

## Last commit
* 3.5.2020
