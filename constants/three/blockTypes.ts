export enum BLOCK_TYPES {
  GRASS = 'GRASS',
  STONE = 'STONE',
  DIORITE = 'DIORITE',
  BEDROCK = 'BEDROCK',
  WOOL_YELLOW = 'WOOL_YELLOW',
  DIAMOND_ORE = 'DIAMOND_ORE',
  GOLD = 'GOLD',
  LAPIS = 'LAPIS',
  CLAY = 'CLAY',
  AIR = 'AIR',
}

export const BLOCK_TEXTURES = {
  'GRASS': 'three/textures/hackAndCraft/atlas.png',
  'STONE': 'three/textures/hackAndCraft/stone.png',
  'DIORITE': 'three/textures/hackAndCraft/stone_diorite.png',
  'BEDROCK': 'three/textures/hackAndCraft/bedrock.png',
  'WOOL_YELLOW': 'three/textures/hackAndCraft/wool_colored_yellow.png',
  'DIAMOND_ORE': 'three/textures/hackAndCraft/diamond_ore.png',
  'GOLD': 'three/textures/hackAndCraft/gold_block.png',
  'LAPIS': 'three/textures/hackAndCraft/lapis_block.png',
  'CLAY': 'three/textures/hackAndCraft/hardened_clay_stained_red.png',
}

const allSidesSame = {
  px: [0, 1, 1, 1, 0, 0, 0, 1],
  nx: [0, 1, 1, 1, 0, 0, 0, 1],
  py: [0, 1, 1, 1, 0, 0, 0, 1],
  ny: [0, 1, 1, 1, 0, 0, 0, 1],
  pz: [0, 1, 1, 1, 0, 0, 0, 1],
  nz: [0, 1, 1, 1, 0, 0, 0, 1]
}

export const BLOC_UV = {
  'GRASS': {
    px: [0, 0.5, 1, 0.5, 0, 0, 1, 0],
    nx: [0, 0.5, 1, 0.5, 0, 0, 1, 0],
    py: [0, 1, 1, 1, 0, 0.5, 1, 0.5],
    ny: [0, 1, 1, 1, 0, 0.5, 1, 0.5],
    pz: [0, 0.5, 1, 0.5, 0, 0, 1, 0],
    nz: [0, 0.5, 1, 0.5, 0, 0, 1, 0]
  },
  'STONE': allSidesSame,
  'DIORITE': allSidesSame,
  'BEDROCK': allSidesSame,
  'WOOL_YELLOW': allSidesSame,
  'DIAMOND_ORE': allSidesSame,
  'GOLD': allSidesSame,
  'CLAY': allSidesSame,
  'LAPIS': allSidesSame,
  'AIR': allSidesSame
}
